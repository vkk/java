
public interface BoundedSetOfString {
	//pre-condition: none
	int getBound();
	//pre-condition: none
	int getSize();
	//pre-condition1: String passed into addElement is not contained in set
	//pre-condition2: addElement will not be called if # elements in set equals set's upper-bound
	void addElement(String element);
	//pre-condition: String passed to be removed will be contained in set
	void removeElement(String element);
	//pre-condition: removeAny will not be invoked on empty set
	String removeAny();
	//pre-condition: none
	boolean IsIn(String element);
}
