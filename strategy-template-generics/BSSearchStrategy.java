import java.util.List;

public class BSSearchStrategy<T extends Comparable<T>> implements
		SearchStrategy<T> {

	public BSSearchStrategy() {

	}

	public boolean search(T query, List<T> objects) {
		int low = 0;
		int high = (objects.size() - 1);
		int mid;

		while (low < high) {
			mid = (low + high) / 2;

			if (objects.get(mid).compareTo(query) == 0)
				return true;
			if (objects.get(mid).compareTo(query) > 0) {
				high = mid - 1;
			}
			else if (objects.get(mid).compareTo(query) < 0) {
				low = mid + 1;
			}
		}
		return false;
	}
}

