import java.util.List;

public interface SearchStrategy <T>{
	
	boolean search(T query, List <T> objects);
}
