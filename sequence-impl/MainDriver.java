
public class MainDriver {

	public static void main(String[] args) {

		System.out.println("Tested 'clear'");
		
		// clear: test case 1
		Sequence s0 = new SequenceImpl();
		s0.clear();
		if(!s0.toString().equals("[]")) {
			System.out.println("expected: [], found: " + s0);
		}
		
		// clear: test case 2
		Sequence s1 = new SequenceImpl();
		s1.add(0, 0);
		s1.clear();
		if(!s1.toString().equals("[]")) {
			System.out.println("expected: [], found: " + s1);
		}
		
		// clear: test case 3
		Sequence s2 = new SequenceImpl();
		s2.add(0, 0);
		s2.add(1, 0);
		s2.clear();
		if(!s2.toString().equals("[]")) {
			System.out.println("expected: [], found: " + s2);
		}
		
		System.out.println("");
		System.out.println("Tested 'add'");
		
		// add: test case 1
		Sequence s3 = new SequenceImpl();
		s3.add(0, 0);
		if(!s3.toString().equals("[0]")) {
			System.out.println("expected: [0], found: " + s3);
		}
		
		// add: test case 2
		Sequence s4 = new SequenceImpl();
		s4.add(0, 0);
		s4.add(1, 1);
		if(!s4.toString().equals("[0, 1]")) {
			System.out.println("expected: [0, 1], found: " + s4);
		}
		
		// add: test case 3
		Sequence s5 = new SequenceImpl();
		s5.add(0, 0);
		s5.add(1, 1);
		s5.add(2, 2);
		if(!s5.toString().equals("[0, 1, 2]")) {
			System.out.println("expected: [0, 1, 2], found: " + s5);
		}
		
		System.out.println("");
		System.out.println("Tested 'remove'");
		
		// remove: test case 1
		Sequence s6 = new SequenceImpl();
		s6.add(0, 0);
		s6.add(1, 1);
		s6.add(2, 2);
		s6.remove(2);
		if(!s6.toString().equals("[0, 1]")) {
			System.out.println("previous: [0, 1, 2], removing: [2], found: " + s6);
		}
		
		// remove: test case 2
		Sequence s7 = new SequenceImpl();
		s7.add(0, 0);
		s7.add(1, 1);
		s7.add(2, 2);
		s7.remove(2);
		s7.remove(1);
		if(!s7.toString().equals("[0]")) {
			System.out.println("previous: [0, 1, 2], removing: [1,2], found: " + s7);
		}
		
		// remove: test case 3
		Sequence s8 = new SequenceImpl();
		s8.add(0, 0);
		s8.add(1, 1);
		s8.add(2, 2);
		s8.remove(2);
		s8.remove(1);
		s8.remove(0);
		if(!s8.toString().equals("[]")) {
			System.out.println("previous: [0, 1, 2], removing: [0, 1, 2], found:  " + s8);
		}
		
		System.out.println("");
		System.out.println("Tested 'getElement'");
		
		// getElement: test case 1
		Sequence s9 = new SequenceImpl();
		s9.add(0, 0);
		s9.add(1, 1);
		s9.add(2, 2);
		if(!s9.getElement(0).toString().equals("0")) {
			System.out.println("expected: 0, found: " + s9.getElement(0).toString());
		}
		
		// getElement: test case 2
		Sequence s10 = new SequenceImpl();
		s10.add(0, 0);
		s10.add(1, 1);
		s10.add(2, 2);
		if(!s10.getElement(1).toString().equals("1")) {
			System.out.println("expected: 1, found: " + s9.getElement(1).toString());
		}
		
		// getElement: test case 3
		Sequence s11 = new SequenceImpl();
		s11.add(0, 0);
		s11.add(1, 1);
		s11.add(2, 2);
		if(!s11.getElement(2).toString().equals("2")) {
			System.out.println("expected: 2, found: " + s9.getElement(2).toString());
		}
		
		System.out.println("");
		System.out.println("Tested 'getLength'");
		
		// getLength: test case 1
		Sequence s12 = new SequenceImpl();
		s12.add(0, 0);
		s12.add(1, 1);
		s12.add(2, 2);
		if(s12.getLength() != 3) {
			System.out.println("expected: 3, found: " + s9.getLength());
		}
		
		// getLength: test case 2
		Sequence s13 = new SequenceImpl();
		s13.add(0, 0);
		s13.add(1, 1);
		s13.add(2, 2);
		s13.remove(2);
		if(s13.getLength() != 2) {
			System.out.println("expected: 2, found: " + s9.getLength());
		}
		
		// getLength: test case 3
		Sequence s14 = new SequenceImpl();
		s14.add(0, 0);
		s14.add(1, 1);
		s14.add(2, 2);
		s14.clear();
		if(s14.getLength() != 0) {
			System.out.println("expected: 0, found: " + s9.getLength());
		}
	}	
}
	
