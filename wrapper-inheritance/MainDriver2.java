
public class MainDriver2 {

	public static void main(String[] args) {
		BoundedSetOfString outer1 = new BoundedSetOfStringImpl(2);
		BoundedSetOfString outer2 = new BoundedSetOfStringChecker(outer1);
			
		outer2.removeElement("Hey");
		outer2.addElement("Hello");
		outer2.addElement("Hello");
		outer2.addElement("Hey");
		outer2.removeElement("Goodbye!");
		outer2.removeElement("Hello");
		outer2.removeElement("Hey");
		outer2.removeAny();
	}
}
