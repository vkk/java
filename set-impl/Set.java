
public interface Set {

	// modeled by: finite set of object
	// initial value: {}

	void clear();
	// post: self = {}

	void add(Object o);
	// pre: o ∈ self
	//
	// preserves: o
	// post: self = #self ∪ {o}

	void remove(Object o);
	// pre: o ∈ self
	// preserves: o
	// post: self = #self - {o}
	
	int removeAny();
	// pre: |self| > 0
	// post: there exists x : object
	//
	// s.t.
	//
	// (x ∈ #self) and (self = #self - {x})

	boolean isIn(Object o);
	// preserves: self
	// post: isIn() = (o ∈ self)

	int getSize();
	// preserves: self
	// post: getSize() = |self|

}
