import java.util.Vector;


public class BoundedSetOfStringImpl 
implements BoundedSetOfString {

	private int m_bound;
	private Vector<String> m_set;
		
	public BoundedSetOfStringImpl(int upper_bound) {
		m_bound = upper_bound;
		m_set = new Vector<String>(upper_bound, 0);
	}
		
	public int getBound() {
		return m_bound;
	}
		
	public int getSize() {
		return m_set.size();
	}

	public void addElement(String element) {
		m_set.addElement(element);
	}

	public void removeElement(String element) {
		m_set.removeElement(element);
	}

	public String removeAny() {
		return m_set.remove(0);
	}

	public boolean IsIn(String element) {
		if(m_set.contains(element)) return true;
		else return false;
	}
}
