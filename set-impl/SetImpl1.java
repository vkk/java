import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector; 

public class SetImpl1 
implements Set {

	private HashSet m_set = new HashSet();

	public SetImpl1() {
		
	}
	
	public void clear() {
		m_set.clear();
	}

	public void add(Object o) {
		m_set.add(o);
	}

	public void remove(Object o) {
		m_set.remove(o);
	}

	public int removeAny() {
		Iterator iter = m_set.iterator();
		Integer temp = (Integer) iter.next();
		iter.remove();
		return temp.intValue();
	}

	public boolean isIn(Object o) {
		return m_set.contains(o);
	}

	public int getSize() {
		return m_set.size();
	}
	
	public String toString() {
		return (m_set.toString());
	}

}
