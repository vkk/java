import java.util.*;

public class BoundedSetOfStringImpl2 
implements BoundedSetOfString {

	private int m_bound;
	private HashSet<String> m_set;
	
	public BoundedSetOfStringImpl2(int upper_bound) {
		m_bound = upper_bound;
		m_set = new HashSet<String>();
	}
		
	public int getBound() {
		return m_bound;
	}
		
	public int getSize() {
		return m_set.size();
	}

	public void addElement(String element) {
		m_set.add(element);
	}

	public void removeElement(String element) {
		m_set.remove(element);
	}

	public String removeAny() {
		Object temp = m_set.iterator().next();
		m_set.remove(temp);
		return temp.toString();
	}

	public boolean IsIn(String element) {
		if(m_set.contains(element)) return true;
		else return false;
	}
}
