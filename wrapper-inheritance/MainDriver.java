
public class MainDriver {

	public static void main(String[] args) {
		BoundedSetOfString outer1 = new BoundedSetOfStringImpl2(2);
		BoundedSetOfString outer2 = new BoundedSetOfStringChecker3(2);
		
		outer2.removeElement("Hey");
		outer2.addElement("Hello");
		outer2.addElement("Hello");
		outer2.addElement("Hey");
		outer2.addElement("More");
		outer2.addElement("Again");
		outer2.removeElement("Goodbye!");
		outer2.removeElement("Hello");
		outer2.removeElement("Hey");
		outer2.removeAny();
	}

}
