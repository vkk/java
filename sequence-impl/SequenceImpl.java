import java.util.Vector;


public class SequenceImpl implements Sequence {
	
	private Vector m_sequence = new Vector();
	
	public SequenceImpl() {
	}

	public void clear() {
		m_sequence.clear();
	}

	public void add(Object o, int pos) {
		m_sequence.add(pos, o);
	}

	public Object remove(int pos) {
		return m_sequence.remove(pos);
	}

	public Object getElement(int pos) {
		return m_sequence.get(pos);
	}

	public int getLength() {
		return m_sequence.size();
	}
	
	public String toString() {
		return (m_sequence.toString());
		
	}

}
