
public class MainDriver {

	public static void main(String[] args) {
		
		SortingAndFindingMachine<String> sm1 = new SortingAndFindingMachineOrder1(new BSSearchStrategy<String>());
		sm1.addObject("e");
		sm1.addObject("x");
		sm1.addObject("a");
		sm1.addObject("q");
		sm1.addObject("b");
		sm1.changeToExtraction();
		System.out.println(sm1.IsIn("q"));
		while(sm1.getSize() > 0) {
			System.out.println(sm1.removeFirst());
		}
	}
}
