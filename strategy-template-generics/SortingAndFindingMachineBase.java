
import java.util.*;

public abstract class SortingAndFindingMachineBase <T extends Comparable<T>>
implements SortingAndFindingMachine <T> {

	private Vector<T> m_objects = new Vector<T>();
	private SearchStrategy<T> m_search;
	
	public SortingAndFindingMachineBase(SearchStrategy<T> search) {
		m_search = search;
	}
	
	public void addObject(T o) {
		int index = 0;
		while((index < m_objects.size()) && (areInOrder(m_objects.elementAt(index), o))) {
			index++;
		}
		m_objects.add(index, o);
	}

	public T removeAny() {
		return m_objects.remove(0);
	}

	public void changeToExtraction() {
	}

	public T removeFirst() {
		return m_objects.remove(0);
	}

	public void clear() {
		m_objects.clear();
	}

	public int getSize() {
		return m_objects.size();
	}

	public boolean IsIn(T o) {
		if(m_search.search(o, m_objects) != false);
		return true;
	}
	
	protected abstract boolean areInOrder(T x, T y);
}
