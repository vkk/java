
public class BoundedSetOfStringChecker 
implements BoundedSetOfString {

	private BoundedSetOfString m_inner;
		
	public BoundedSetOfStringChecker(BoundedSetOfString inner) {
		m_inner = inner;
	}
		
	public int getBound() {
		return m_inner.getBound();
	}
		
	public int getSize() {
		return m_inner.getSize();
	}

	public void addElement(String element) {
		//pre-condition1: String called into addElement is not contained in set
		if((m_inner.IsIn(element))) { 
			System.out.println("Error: '" + element + "' is contained in the set already."); 
		    return;
		}
		//pre-condition2: addElement will not be called if # elements in set equals set's upper-bound
		if((m_inner.getSize() == getBound())) {
			System.out.println("Error: The number of elements in the set equals set upper-bound.");
			return;
		}
		m_inner.addElement(element);
	}

	public void removeElement(String element) {
		 //pre-condition: String called to be removed will be contained in set
		if((!m_inner.IsIn(element))) {
			System.out.println("Error: '" + element + "' is not in the set.");
			return;
		}
		m_inner.removeElement(element);
	}

	public String removeAny() {
		//pre-condition: removeAny will not be invoked on empty set
		if((m_inner.getSize() == 0)) {
			System.out.println("Error: The set is empty.");
			return null;
		}
		return m_inner.removeAny();
	}

	public boolean IsIn(String element) {
		if(m_inner.IsIn(element)) return true;
		else return false;
	}
}
