
public class BoundedSetOfStringChecker2 
extends BoundedSetOfStringImpl {
					
	public BoundedSetOfStringChecker2(int upper_bound) {
		super(upper_bound);
	}

	public void addElement(String element) {
		//pre-condition1: String called into addElement is not contained in set
		if((super.IsIn(element))) { 
			System.out.println("Error: '" + element + "' is contained in the set already."); 
		    return;
		}
		//pre-condition2: addElement will not be called if # elements in set equals set's upper-bound
		if((super.getSize() == getBound())) {
			System.out.println("Error: The number of elements in the set equals set upper-bound.");
			return;
		}
		super.addElement(element);
	}

	public void removeElement(String element) {
		 //pre-condition: String called to be removed will be contained in set
		 if((!super.IsIn(element))) {
			System.out.println("Error: '" + element + "' is not in the set.");
			return;
		}
		super.removeElement(element);
	}

	public String removeAny() {
		//pre-condition: removeAny will not be invoked on empty set
		if((super.getSize() == 0)) {
			System.out.println("Error: The set is empty.");
			return null;
		}
		return super.removeAny();
	}

	public boolean IsIn(String element) {
		if(super.IsIn(element)) return true;
		else return false;
	}
}
