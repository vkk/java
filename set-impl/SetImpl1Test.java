import junit.framework.TestCase;

public class SetImpl1Test 
extends TestCase {

	private Set s0;
	private Set s1;
	private Set s2;
	private Set s3;
	private String pres0;
	private String pres1;
	private String pres2;
	private String pres3;
	private int anyInt;
	private int anyInt1;
	private int anyInt2;
	
	public SetImpl1Test(String arg0) {
		super(arg0);
	}

	protected void setUp() throws Exception {
		s0 = new SetImpl1();
		s1 = new SetImpl1();
		s2 = new SetImpl1();
		s3 = new SetImpl1();
		
		s0.add(0);
		s1.add(1);
		s2.add(1);
		s2.add(2);
		s3.add(1);
		s3.add(2);
		s3.add(3);
		
		pres0 = s0.toString();
		pres1 = s1.toString();
		pres2 = s2.toString();
		pres3 = s3.toString();
	}
	
	public void testConstructor() {
		Set test = new SetImpl1();
		assertEquals("[]", test.toString());
	} 
	
	public void testClear() {
		s0.clear();
		assertEquals("[]", s0.toString());
		
		s1.clear();
		assertEquals("[]", s1.toString());
		
		s2.clear();
		assertEquals("[]", s2.toString());
	} 

	public void testAdd() {
		Set test = new SetImpl1();
		s1.add(test);
		s1.add(2);
		assertEquals("[1, 2, []]", s1.toString()); 
		
		
		Set test1 = new SetImpl1();
		s2.add(test1);
		s2.add(3);
		assertEquals("[1, 2, 3, []]", s2.toString());
		
		Set test2 = new SetImpl1();
		s3.add(test2);
		s3.add(4);
		assertEquals("[1, 2, 3, 4, []]", s3.toString());
	}

	public void testRemove() {
		// test to remove all
		s1.remove(1);
		assertEquals("[]", s1.toString());
		
		// test to remove last
		s2.remove(2);
		assertEquals("[1]", s2.toString());
		
		// test to remove first and middle
		s3.remove(1);
		s3.remove(2);
		assertEquals("[3]", s3.toString());
	}

	public void testRemoveAny() {
		anyInt = s1.removeAny();
		assertFalse(s1.isIn(anyInt));
		s1.add(anyInt);
		assertTrue(s1.isIn(anyInt));
		assertEquals("[1]", s1.toString());

		anyInt1 = s2.removeAny();
		assertFalse(s2.isIn(anyInt1));
		s2.add(anyInt1);
		assertTrue(s2.isIn(anyInt1));
		assertEquals("[1, 2]", s2.toString());
		
		anyInt2 = s3.removeAny();
		assertFalse(s3.isIn(anyInt2));
		s3.add(anyInt2);
		assertTrue(s3.isIn(anyInt2));
		assertEquals("[1, 2, 3]", s3.toString());
	}

	public void testIsIn() {
		assertTrue(s1.isIn(1));
		assertEquals("[1]", s1.toString());
		
		assertTrue(s2.isIn(1));
		assertEquals("[1, 2]", s2.toString());
		
		assertTrue(s3.isIn(2));
		assertEquals("[1, 2, 3]", s3.toString());
	}

	public void testGetSize() {
		assertEquals(1, s1.getSize());
		assertEquals("[1]", s1.toString());
		
		assertEquals(2, s2.getSize());
		assertEquals("[1, 2]", s2.toString());
		
		assertEquals(3, s3.getSize());
		assertEquals("[1, 2, 3]", s3.toString());
	}
}
