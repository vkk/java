
public interface Sequence {
	// modeled by: string of object 3	
	// initial value: self = <> 	
	
	void clear(); 	
	// post: self = <> 	
	
	void add(Object o, int pos); 	
	// pre: 0 <= pos <= |self|
	// preserves: o, pos 	
	// post: 
		// there exists l, r : string of object 
		// s.t. 
		// #self = l * r and 
		// |l| = pos and 
		// self = l * <o> * r
		// ...alternatively:
	// post:
		// self = Prt_Btwn(0, pos, #self) * <o> * Prt_Btwn(pos, |#self|, #self)
	
	Object remove(int pos);
	// pre: 0 <= pos < |self|
	// preserves: pos
	// post: 
		// there exists x : object, l, r : string of object
		// s.t. 
		// #self = l * <x> * r and 
		// |l| = pos and
		// self = l * r and 
		// remove() = x
		// ...alternatively:
	// post:
		// self = Prt_Btwn(0, pos, #self) * Prt_Btwn(pos + 1, |#self|, #self) and 
		// remove() = DeString(Prt_Btwn(pos, pos + 1, #self))
	
	Object getElement(int pos);
		// pre: self != <> 
		// preserves: self, pos 
		// post:
			// there exists x : object, l, r : string of object 
			// s.t. 
			// #self = l * <x> * r and 
			// |l| = pos and
			// getElement() = x
			// ...alternatively:
		// post:
			// remove() = DeString(Prt_Btwn(pos, pos + 1, #self))
		
	int getLength(); 
		// preserves: self
		// post: getLength() = |#self|
}
