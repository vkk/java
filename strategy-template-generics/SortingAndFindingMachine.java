
public interface SortingAndFindingMachine <T>{

	void addObject(T o);
	T removeAny();
	void changeToExtraction();
	T removeFirst();
	void clear();
	int getSize();
	boolean IsIn(T o);
}
