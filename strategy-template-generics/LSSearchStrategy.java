import java.util.List;
import java.util.Vector;

public class LSSearchStrategy <T extends Comparable<T>> 
implements SearchStrategy<T>{

	private Vector<T> m_objects = new Vector<T>();
	
	public LSSearchStrategy() {
		
	}
	
	public boolean search(T query, List <T> objects) {
		int index = 0;
		while((index < m_objects.size())) {
			if(objects.get(index).equals(query)) return true;
			index++;
		}
		return false;
	}

}
