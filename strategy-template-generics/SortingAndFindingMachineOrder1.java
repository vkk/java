
public class SortingAndFindingMachineOrder1 <T extends Comparable<T>>
extends SortingAndFindingMachineBase <T>{

	public SortingAndFindingMachineOrder1(SearchStrategy<T> search) {
		super(search);
	}

	protected boolean areInOrder(T i, T j) {
		return (i.compareTo(j)) >= (j.compareTo(i)); 
	}

}
